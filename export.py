import argparse
import re
import json
import praw

from typing import Optional
from pathlib import Path

from submissions import Submissions
from utils import read_pipelined


class Exporter:
    """
    Download and export links in submissions.
    """

    submissions: Submissions
    domains: Optional[list[str]]

    def __init__(self, submissions: Submissions, domains: Optional[list[str]] = None) -> None:
        self.submissions = submissions
        self.domains = domains

    def export(self, export_path: Path, formats: list[str]) -> None:
        """
        Export submissions in the specified file formats.
        Suffixes will appropriately be appended to the export path.
        """

        if 'json' in formats:
            json_data = self._export_json()

            with open(export_path.with_suffix('.json'), mode='w') as f:
                json.dump(json_data, f, indent=4)

        if 'markdown' in formats:
            md = self._export_markdown()

            with open(export_path.with_suffix('.md'), mode='w') as f:
                f.write(md)

    def _export_json(self) -> dict:
        """
        Export submission data to JSON.
        """

        json_data: list[dict] = []

        for submission_data in submissions.data.values():
            links = submission_data.match_links(domains=self.domains)

            json_data.append({
                'url': submission_data.submission.url,
                'has_comments': submission_data.has_comments,
                'links': list(links),
            })

        return json_data

    def _export_markdown(self) -> str:
        """
        Export submission data to Markdown.
        This makes it easier to manually sift through.

        Submissions are listed based on whether
        they have no comments, no links, or do have links.
        """

        md = '## Submissions'

        md += '\n\n### No comments'
        for submission_data in self.submissions.data.values():
            if submission_data.has_comments:
                continue
            md += f'\n- {submission_data.submission.url}'

        md += '\n\n### No links'
        for submission_data in self.submissions.data.values():
            if submission_data.links:
                continue
            md += f'\n- {submission_data.submission.url}'

        md += '\n\n### With links'

        results = self.submissions.search(domains=self.domains)
        for submission_data, links in results.items():
            md += f'\n- {submission_data.submission.url}'

            for link in links:
                md += f'\n  - {link}'

        return md


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--client-id',
        type=str,
        required=True,
        help='Reddit app client ID',
    )
    parser.add_argument(
        '--client-secret',
        type=str,
        required=True,
        help='Reddit app client secret',
    )
    parser.add_argument(
        '--export-path',
        type=Path,
        required=True,
        help='Where to export data. Suffixes will be '
             'appended depending on the file format.'
    )

    parser.add_argument(
        '-e', '--export-format',
        dest='export_formats',
        type=str,
        action='append',
        choices=('json', 'markdown'),
        required=True,
        help='File formats to which to export the data.',
    )

    parser.add_argument(
        '-d', '--domain',
        dest='domains',
        type=str,
        action='append',
        default=None,
        help='Domains for filtering links',
    )

    args = parser.parse_args()

    export_path = args.export_path.absolute()

    submission_urls: list[str] = list(read_pipelined())

    if not submission_urls:
        print('No submission URLs were provided.')
        exit()


    reddit = praw.Reddit(
        client_id=args.client_id,
        client_secret=args.client_secret,
        user_agent='Chrome/51.0.2704.106',
    )

    submissions = Submissions(reddit)
    submissions.load(*submission_urls)

    exporter = Exporter(submissions, args.domains)
    exporter.export(export_path, args.export_formats)
