import sys
import time

from typing import Union, Generator, Optional
from urllib.parse import urlparse
from bs4 import BeautifulSoup

import praw
from praw.models.comment_forest import CommentForest
from praw.models import Comment, MoreComments


def get_comments(comments: Union[CommentForest, MoreComments]) \
        -> Generator[Comment, None, None]:
    """
    Get all comments in a comment hierarchy.
    """

    for comment in comments:
        # Wait a little to avoid rate limitting.
        # I don't know if this is enough but it's something.
        time.sleep(0.05)

        if isinstance(comment, MoreComments):
            yield from get_comments(comment.comments())
        else:
            yield comment

            # Include all replies of the comment.
            yield from get_comments(comment.replies)


def is_subdomain(subdomain: str, domain: str) -> bool:
    """
    Check if one domain is a subdomain of another.
    """


    subdomain = urlparse(subdomain)
    domain = urlparse(domain)

    if subdomain.scheme != domain.scheme:
        return False

    subdomain_parts = subdomain.netloc.split('.')[::-1]
    domain_parts = domain.netloc.split('.')[::-1]

    if len(domain_parts) > len(subdomain_parts):
        return False

    return all(s == d for s, d in zip(subdomain_parts, domain_parts))


def get_links(body: str) -> Generator[str, None, None]:
    """
    Get all links in an HTML body.
    """

    soup = BeautifulSoup(body, 'html.parser')

    for a in soup.find_all('a', href=True):
        yield a['href']


def read_pipelined() -> Generator[str, None, None]:
    """
    Read lines that have been pipelined into the script.
    """

    while True:
        if not sys.stdin.readable():
            break

        line = sys.stdin.readline().strip()

        if not line:
            break

        yield line
