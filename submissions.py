import re

from typing import Union, Optional
from tqdm import tqdm

import praw
from praw import Reddit
from praw.models import Submission, Comment

from utils import get_comments, is_subdomain, get_links


REDDITOR_BOTS = ['AutoModerator']


class SubmissionData:
    """
    Data about a submission,
    such as its comments and links inside
    """

    submission: Submission
    comments: list[Comment]
    links: set[str]

    def __init__(self, submission: Submission) -> None:
        self.submission = submission
        self.comments = list(tqdm(get_comments(self.submission.comments)))

        self.links = set(get_links(self.submission.selftext))
        for comment in self.comments:
            self.links |= set(get_links(comment.body_html))

    @property
    def has_comments(self) -> bool:
        """
        Whether the submission has at least one comment by a human
        """
        for comment in self.comments:
            if comment.author not in REDDITOR_BOTS:
                return True
        return False

    def match_links(self, domains: Optional[list[str]] = None) -> set[str]:
        """
        Find links under any of the domains.
        """

        if domains is None:
            return set(self.links)

        matched_links: set[str] = set()

        for link in self.links:
            if any(is_subdomain(link, domain) for domain in domains):
                matched_links.add(link)
  
        return matched_links


class Submissions:
    """
    Download and manage data from multiple submissions.
    """

    reddit: Reddit
    data: dict[str, SubmissionData]

    def __init__(self, reddit: Reddit) -> None:
        self.reddit = reddit
        self.data: dict[str, SubmissionData] = {}

    def load(self, *urls: str) -> None:
        """
        Load submissions by URL.
        """

        n = len(urls)
        n_length = len(str(n))

        for i, url in enumerate(urls):
            submission = self.reddit.submission(url=url)

            print(f'[{i+1:0>{n_length}} / {n}]  {submission.title}')

            if submission.url in self.data:
                continue

            data = SubmissionData(submission)
            self.data[submission.url] = data

    def search(self, domains: Optional[Union[str, list[str]]] = None) \
            -> dict[SubmissionData, set[str]]:
        """
        Search for links under any of the domains.
        """

        if domains is None:
            return {data: set(data.links) for data in self.data.values()}

        if not isinstance(domains, list):
            domains = [domains]

        matches: dict[SubmissionData, set[str]] = {}

        for submission_data in self.data.values():
            links = submission_data.match_links(domains=domains)

            if links:
                matches[submission_data] = links

        return matches
